from django.urls import path,include,re_path
from .views import UploadTest,Test,MyCaptcha,get_url,get_access_token,get_tiny,alipay_return,page1,miaosha,imageupload,test_socket,test_websocket,frontupload,test_count,MyTest

#导入模板解析类
from django.views.generic import TemplateView


#声明命名空间
app_name = 'md'

urlpatterns = [

    #后台首页
    path('',TemplateView.as_view(template_name='md_admin/index.html')),
    #websocket
    path('socket_test',TemplateView.as_view(template_name='md_admin/socket.html')),
    path('websocket_test',TemplateView.as_view(template_name='md_admin/socket_push.html')),
    path('test_socket',test_socket),
    path('test_websocket',test_websocket),
    #定义上传逻辑
    path('upload_img',UploadTest.as_view()),
    path('test',Test.as_view()),
    path('mycaptcha',MyCaptcha.as_view()),
    path('front_upload',frontupload),
    #定义登录页面
    path('login',TemplateView.as_view(template_name='md_admin/login.html')),
    #weibo geturl
    path('geturl',get_url),
    #weibo 回调
    path('weibo',get_access_token),
    path('tinymice',get_tiny),
    path('alipayreturn',alipay_return),
    path('page1',page1),
    path('miaosha',miaosha),
    path('imageupload',imageupload),
    path('test_count',test_count),
    path('mytest',MyTest.as_view())

]