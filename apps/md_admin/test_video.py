import ffmpeg

def get_frames_by_times():
    times = [1,10]
    for time in times:
        input_file = './test.mp4'
        output_file = './image-' + str(time) + '.jpg'
        out, err = (
            ffmpeg
                .input(input_file, ss=time)
                .output(output_file, vframes='1', f='image2')
                .run(quiet=False, overwrite_output=True)
        )
        if out == b'':
            print('do nothing')

if __name__ == "__main__":

    # info = ffmpeg.probe("./test.mp4")
    # vs = next(c for c in info['streams'] if c['codec_type'] == 'video')
    # duration_secs = float(vs['duration'])
    # width = vs['width']
    # height = vs['height']
    # print(duration_secs)


    #print(info)
    get_frames_by_times()