from django.db import models

#定义新闻类
class News(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
    content = models.TextField()
    pd = models.IntegerField()

    #定义表名
    class Meta:
        db_table = 'news'