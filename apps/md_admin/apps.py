from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'md_admin'
