from django.shortcuts import render
#导包
from django.shortcuts import render,redirect
#导包
from django.http import HttpResponse,HttpResponseRedirect
#导入类视图
from django.views import View
import os
from settings import UPLOAD_ROOT
import json
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.serializers import serialize
from .models import News

#导入绘图库
from PIL import ImageDraw
#导入绘图字体库
from PIL import ImageFont
#导入图片库
from PIL import Image

#导入io库
import io

#导入随机库
import random
import requests

import time

from .pay import AliPay

from django.db import connection

import redis

from dwebsocket.decorators import accept_websocket


r = redis.Redis(host='localhost', port=6379)


#定义验证接口
from django.http import JsonResponse
import jwt
def auth_required():
    def decorator(view_func):
        def _wrapped_view(self,request, *args, **kwargs):


            try:
                auth = request.META.get('HTTP_AUTHORIZATION').split()
            except AttributeError:
                return HttpResponse('没权限')
            
            try:
                dict = jwt.decode(auth[1], settings.SECRET_KEY, algorithms=['HS256'])
                username = dict.get('data').get('username')
            except jwt.ExpiredSignatureError:
                return JsonResponse({"status_code": 401, "message": "Token expired"})
            except jwt.InvalidTokenError:
                return JsonResponse({"status_code": 401, "message": "Invalid token"})
            except Exception as e:
                return JsonResponse({"status_code": 401, "message": "Can not get user object"})

            return view_func(request, *args, **kwargs)

        return _wrapped_view

    return decorator


class MyTest(View):
    @auth_required()
    def get(self,request):

        return HttpResponse('123')



#定义防止作弊的访问计数器
def limit_count():
    #定义内部方法
    def rate_limit(func):
        def func_limit(self,request):

            if 'HTTP_X_FORWARDED_FOR' in request.META:
                ip = request.META.get('HTTP_X_FORWARDED_FOR')
            else:
                ip = request.META.get('REMOTE_ADDR')

            res = r.get('127.0.0.1')

            print(res)

            if res:
                print('已经来过了，不加一')
            else:
                r.set('127.0.0.1','127.0.0.1',10)
                
                pcount = r.get('pid')
                if not pcount:
                    pcount = 0
                pcount = int(pcount)
                print(pcount)
                pcount += 1
                r.set('pid',pcount)


                print('访问数加一')
                

            ret = func(self,request)
            return ret
        return func_limit
    return rate_limit

@limit_count()
def test_count(request):

    return HttpResponse('123')





#接受前端信息
@accept_websocket
def test_socket(request):
    if request.is_websocket():
        for message in request.websocket:
            c=str(message,encoding='utf-8')
            print(c)
            request.websocket.send(message)

@accept_websocket
def test_websocket(request):
    if request.is_websocket():
        while 1:
            time.sleep(1) ## 向前端发送时间
            dit = {
                'time':time.strftime('%Y.%m.%d %H:%M:%S',time.localtime(time.time()))
            }
            request.websocket.send(json.dumps(dit))




#定义过载
def limit_handler():
    """
    return True: 允许; False: 拒绝
    """
    amount_limit = 3  # 限制数量
    keyname = 'limit123'  # redis key name
    incr_amount = 1  # 每次增加数量

    # 判断key是否存在
    if not r.exists(keyname):
        # 为了方便测试，这里设置默认初始值为95
        # setnx可以防止并发时多次设置key
        r.setnx(keyname, 0)

    # 数据插入后再判断是否大于限制数
    if r.incrby(keyname, incr_amount) <= amount_limit:
        return True

    return False


#定义秒杀接口
def miaosha(request):
    res_one = News.objects.get(pk=1)
    if limit_handler():
    #if res_one.pd > 0:
        time.sleep(5)
        # res_one.pd = res_one.pd - 1
        # res_one.save()
        with connection.cursor() as c:
            c.execute(' update news set pd = pd - 1 where id = 1 ')
        return HttpResponse('ok')
    else:
        return HttpResponse('没有了')




#初始化阿里支付对象
def get_ali_object():
    # 沙箱环境地址：https://openhome.alipay.com/platform/appDaily.htm?tab=info
    app_id = "2016092600603658"  #  APPID （沙箱应用）

    # 支付完成后，支付偷偷向这里地址发送一个post请求，识别公网IP,如果是 192.168.20.13局域网IP ,支付宝找不到，def page2() 接收不到这个请求
    notify_url = "http://localhost:8000/md_admin/alipayreturn"

    # 支付完成后，跳转的地址。
    return_url = "http://localhost:8000/md_admin/alipayreturn"

    merchant_private_key_path = "c:/Users/liuyue/www/md/keys/app_private_2048.txt" # 应用私钥
    alipay_public_key_path = "c:/Users/liuyue/www/md/keys/alipay_public_2048.txt"  # 支付宝公钥

    alipay = AliPay(
        appid=app_id,
        app_notify_url=notify_url,
        return_url=return_url,
        app_private_key_path=merchant_private_key_path,
        alipay_public_key_path=alipay_public_key_path,  # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥
        debug=True,  # 默认False,
    )
    return alipay



def page1(request):

    if request.method == "POST":

        # 根据当前用户的配置，生成URL，并跳转。
        money = float(request.POST.get('money'))

        alipay = get_ali_object()

        # 生成支付的url
        query_params = alipay.direct_pay(
            subject="test",  # 商品简单描述
            out_trade_no="myorder" + str(time.time()),  # 用户购买的商品订单号（每次不一样） 20180301073422891
            total_amount=money,  # 交易金额(单位: 元 保留俩位小数)
        )

        pay_url = "https://openapi.alipaydev.com/gateway.do?{0}".format(query_params)  # 支付宝网关地址（沙箱应用）

        return redirect(pay_url)
    else:
        return render(request,'md_admin/page1.html')


def alipay_return(request):
    alipay = get_ali_object()
    params = request.GET.dict()
    sign = params.pop('sign', None)
    status = alipay.verify(params, sign)
    print('==================开始==================')
    print('GET验证', status)
    print('==================结束==================')
    return HttpResponse('支付成功')



def get_url(request):
    #微博接口地址
    weibo_auth_url = "https://api.weibo.com/oauth2/authorize"
    #回调网址
    redirect_url = "http://127.0.0.1:8000/md_admin/weibo"
    #应用id
    client_id = "2636039333"
    #组合url
    auth_url = weibo_auth_url + "?client_id={client_id}&redirect_uri={re_url}".format(client_id=client_id,
                                                                                      re_url=redirect_url)
    return HttpResponse(auth_url)


def get_access_token(request):
    #获取回调的code
    code = request.GET.get('code')
    #微博认证地址
    access_token_url = "https://api.weibo.com/oauth2/access_token"
    #参数
    re_dict = requests.post(access_token_url,data={
        "client_id": '2636039333',
        "client_secret": "4e2fbdb39432c31dc5c2f90be3afa5ce",
        "grant_type": "authorization_code",
        "code": code,
        "redirect_uri": "http://127.0.0.1:8000/md_admin/weibo",
    })


    re_dict = re_dict.text
    re_dict = eval(re_dict)
    print(re_dict.get('uid'))



    return HttpResponse(re_dict)

#定义验证码类
class MyCaptcha(View):
    #定义随机颜色方法
    def get_random_color(self):
        
        R = random.randrange(255)
        G = random.randrange(255)
        B = random.randrange(255)

        return (R,G,B)

    #定义随机验证码
    def get(self,request):
        #定义背景颜色
        bg_color = self.get_random_color()
        #定义画布大小 宽，高
        img_size = (150,80)
        #定义画笔 颜色种类,画布，背景颜色
        image = Image.new("RGB",img_size,bg_color)
        #定义画笔对象 图片对象,颜色类型
        draw = ImageDraw.Draw(image,'RGB')
        #定义随机字符
        source = '0123456789asdfghjkl'
        #定义四个字符
        #定义好容器，用来接收随机字符串
        code_str = ''
        for i in range(4):
            #获取随机颜色 字体颜色
            text_color = self.get_random_color()
            #获取随机字符串
            tmp_num = random.randrange(len(source))
            #获取字符集
            random_str = source[tmp_num]
            #将随机生成的字符串添加到容器中
            code_str += random_str
            #将字符画到画布上 坐标，字符串，字符串颜色，字体
            #导入系统真实字体,字号
            my_font = ImageFont.truetype("c:\\windows\\Fonts\\arial.ttf",20)
            draw.text((10+30*i,20),random_str,text_color,font=my_font)
        #使用io获取一个缓存区
        buf = io.BytesIO()
        #将图片保存到缓存区
        image.save(buf,'png')

        #将随机码存储到session中
        request.session['code'] = code_str

        #第二个参数声明头部信息
        return HttpResponse(buf.getvalue(),'image/png')


def get_tiny(request):

    return render(request,'md_admin/tiny.html')

#定义restful接口
class Test(APIView):
    def get(self,request):
        #data = {'name':['1','2','3']}
        #news = News(content=json.dumps(data))
        #news.save()
        #res1 = News.objects.get(pk=4)
        #print(json.loads(res1.content))
        res = News.objects.filter()
        b = serialize('json',res,ensure_ascii=False)
        return Response(b)

#定义上传视图类
class UploadTest(View):

    #定义上传方法
    def post(self,request):
        #接收文件，以对象的形式
        img = request.FILES.get("file")
        print(request.POST.get('name','未收到参数'))
        #文件名称是name属性
        #建立文件流对象
        f = open(os.path.join(UPLOAD_ROOT,'',img.name),'wb')
        #写文件 遍历图片文件流
        for chunk in img.chunks():
            f.write(chunk)
        #关闭文件流
        f.close()
        return HttpResponse(json.dumps({'status':'ok'},ensure_ascii=False),content_type='application/json')



#定义上传视频方法
def uploadmp4(request):
    if request.method == 'POST':
        item = {}
        file = request.FILES.get('file')
        f = open(os.path.join(UPLOAD_ROOT,'',file.name),'wb')

        item['message'] = '上传成功'
        item['url'] = 'http://localhost:8000/upload/'+ file.name
        item['error'] = 0
        #写文件 遍历文件流
        for chunk in file.chunks():
            f.write(chunk)
        return HttpResponse(json.dumps(item,ensure_ascii=False),content_type='application/json')


#kindeditor上传方法
def imageupload(request):
    if request.method == 'POST':
        item = {}
        file = request.FILES.get('imgFile')
        f = open(os.path.join(UPLOAD_ROOT,'',file.name),'wb')

        item['message'] = '上传成功'
        item['url'] = 'http://localhost:8000/upload/'+ file.name
        item['error'] = 0
        #写文件 遍历图片文件流
        for chunk in file.chunks():
            f.write(chunk)
        return HttpResponse(json.dumps(item,ensure_ascii=False),content_type='application/json')



#跨域上传方法
def frontupload(request):
    if request.method == 'POST':
        item = {}
        file = request.FILES.get('imgFile')
        #定义跳转网址，就是前端伪造好的页面
        callBackPath = 'http://localhost:8080/redirect.html'
        f = open(os.path.join(UPLOAD_ROOT,'',file.name),'wb')
        item['message'] = '上传成功'
        item['url'] = 'http://localhost:8000/upload/'+ file.name
        item['error'] = 0
        #写文件 遍历图片文件流
        for chunk in file.chunks():
            f.write(chunk)
        return HttpResponseRedirect(callBackPath + "?error=0&url="+item['url'])