#导入send_mail
from django.core.mail import send_mail
#导入默认发送邮箱
from settings import DEFAULT_FROM_EMAIL
#导入时间模块
import time


#定义发送邮件类

class SendMail(object):

    #初始化方法
    def __init__(self,title,body,email_to,from_email):
        self.title = title
        self.body = body
        self.email_to = email_to
        self.from_email = from_email

    #定义发送方法
    def do_send_mail(self):
        #开始发邮件
        send_status = send_mail(self.title,self.body,self.from_email,self.email_to)
        if send_status:
            return '发送成功'+time.ctime()
        else:
            return '发送失败'+time.ctime()


