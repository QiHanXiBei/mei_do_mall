import requests
PROXY_POOL_URL = 'http://localhost:5555/random'
def get_proxy():
    try:
        response = requests.get(PROXY_POOL_URL)
        if response.status_code == 200:
            return response.text
    except ConnectionError:
        return None
if __name__ == "__main__":
    my_ip = get_proxy()

    #定义代理
    proxie = { 
        'http' : my_ip
    }

    #使用代理
    response = requests.get('http://www.baidu.com',proxies=proxie)
    print(response.text)

