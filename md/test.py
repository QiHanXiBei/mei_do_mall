import jieba
from jieba import analyse
import jwt


encoded_jwt = jwt.encode({'username':'adimn'},'secret_key',algorithm='HS256')

print(jwt.decode(encoded_jwt,'secret_key',algorithms=['HS256']))




# seg_list = jieba.cut("我来到北京清华大学", cut_all=True)
# print("Full Mode: " + "/ ".join(seg_list))  # 全模式

# seg_list = jieba.cut("我来到北京清华大学", cut_all=False)
# print("Default Mode: " + "/ ".join(seg_list))  # 精确模式

# seg_list = jieba.cut("他来到了网易杭研大厦")  # 默认是精确模式
# print(", ".join(seg_list))

# seg_list = jieba.cut_for_search("小明硕士毕业于中国科学院计算所，后在日本京都大学深造")  # 搜索引擎模式
# print(", ".join(seg_list))


# def two_sum(mylist,target):
#     rlist = []
#     count = len(mylist)
#     for i in range(0,count):
#         for j in range(i+1,count):
#             sum = mylist[i] + mylist[j]
#             if sum == target:
#                 rlist.append(i)
#                 rlist.append(j)
#                 break
#     return rlist



def get_word():
    # 引入TextRank关键词抽取接口
    textrank = analyse.textrank
    
    # 原始文本
    text = "线程是程序执行时的最小单位，它是进程的一个执行流，\
            是CPU调度和分派的基本单位，一个进程可以由很多个线程组成，\
            线程间共享进程的所有资源，每个线程有自己的堆栈和局部变量。\
            线程由CPU独立调度执行，在多CPU环境下就允许多个线程同时运行。\
            同样多线程也可以实现并发操作，每个请求分配一个线程来处理。"
    
    # 基于TextRank算法进行关键词抽取
    keywords = textrank(text)
    # 输出抽取出的关键词
    for keyword in keywords:
        print(keyword + "/")
