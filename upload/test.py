import configparser
import os

upload_path = os.path.dirname(os.path.dirname(__file__))+"/static/api.conf"

cf = configparser.ConfigParser()
cf.read(upload_path, encoding='utf-8')
sections = cf.sections()
print(sections)