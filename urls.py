"""md URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include,re_path
#导入文件路由库
from django.views.static import serve
#导入配置文件的路径
from settings import UPLOAD_ROOT

urlpatterns = [
    #定义图片超链接路由
    re_path('^upload/(?P<path>.*)$',serve,{'document_root':UPLOAD_ROOT}),
    path('admin/', admin.site.urls),
    #导入子应用路由
    path('md_admin/',include('apps.md_admin.urls'))
]
