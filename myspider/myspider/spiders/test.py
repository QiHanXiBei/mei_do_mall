#导包
import scrapy
import os
from scrapy_redis.spiders import RedisSpider

#定义抓取类
#class Test(scrapy.Spider):
class Test(RedisSpider):

    #定义爬虫名称，和命令行运行时的名称吻合
    name = "test"

    #定义redis的key
    redis_key = 'test:start_urls'

    #定义头部信息
    haders = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/73.0.3683.86 Chrome/73.0.3683.86 Safari/537.36'
    }

    def parse(self, response):
        print(response.url)
        pass

    # #定义回调方法
    # def parse(self, response):
    #     #将抓取页面保存为文件
    #     page = response.url.split("/")[-2]
    #     filename = 'test-%s.html' % page
    #     if not os.path.exists(filename):
    #         with open(filename, 'wb') as f:
    #             f.write(response.body)
    #     self.log('Saved file %s' % filename)


    #     #匹配规则

    #     content_left_div = response.xpath('//*[@id="content-left"]')
    #     content_list_div = content_left_div.xpath('./div')

    #     for content_div in content_list_div:
    #         yield {
    #             'author': content_div.xpath('./div/a[2]/h2/text()').get(),
    #             'content': content_div.xpath('./a/div/span/text()').getall(),
    #         }

    # #定义列表方法
    # def start_requests(self):
    #     urls = [
    #         'https://www.qiushibaike.com/text/page/1/',
    #         'https://www.qiushibaike.com/text/page/2/',
    #     ]
    #     for url in urls:
    #         #如果想使用代理 可以加入代理参数 meta
    #         #meta={'proxy': 'http://proxy.yourproxy:8001'}

    #         #抓取方法
    #         yield scrapy.Request(url=url, callback=self.parse,headers=self.haders)